#Prompt
autoload -U colors && colors	
PS1="%F{red}[%n@%f%F{blue}%M %~]%f "

#COMPLETION
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

#Vi
bindkey -v
export KEYTIMEOUT=1

#History
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=~/.zhist
setopt INC_APPEND_HISTORY_TIME

#Opts 
setopt autocd
#Exports
export VISUAL=nvim;
export EDITOR=nvim;
#Aliases
alias nc='nvim ~/.config/nvim/init.vim'
alias dwme='nvim ~/wm/dwm/config.h'
alias dwmcd='cd ~/wm/dwm/'
alias dwmb='cd ~/wm/dwm/ && sudo make install && make clean && cd -'
alias zshe='nvim ~/.zshrc'
alias s='sudo systemctl'
alias pm='sudo pacman'
alias zc='nvim ~/.zshrc'
alias kx='killall xinit'
#Only use if you want exa, as an ls replacement.
#alias ls='exa -1'
alias lss='exa -la | grep'

#DT's Colorscripts
#colorscript -r


