let g:coc_user_config = {}
call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'digitaltoad/vim-pug'
Plug 'mattn/emmet-vim'
Plug 'skammer/vim-css-color'
Plug 'vim-airline/vim-airline'
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': './install.sh'
    \ }
call plug#end()

let g:airline#extensions#tabline#enabled = 1

colorscheme gruvbox

nmap tn :tabnew<CR>
nmap tc :tabclose<CR>

nmap nt :NERDTree<CR>
nmap ntc :NERDTreeClose<CR>

nmap <Shift>de :%d<CR> 
let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'


