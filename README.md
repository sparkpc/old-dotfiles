# My dotfiles and rice for Arch Linux

## Dotfiles included

+ Qtile
+ Zsh
+ Nvim
+ Doom Emacs
+ Xinitrc
+ Xresources 
+ Alacritty 
+ Sxhkd

## Things I plan to add
- [x] I3 
- [x] I3blocks 
- [x] Full BSPWM config
- [x] Polybar
- [ ] Deploy Script
- [ ] Normal Vim
- [ ] AwesomeWM
- [x] XMonad
- [x] XMobar

## Things I want to redo 
- [x] Qtile
